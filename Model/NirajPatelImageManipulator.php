<?php

namespace Mbs\ContactWithAvatar\Model;

use Magento\Framework\App\Filesystem\DirectoryList;

class NirajPatelImageManipulator
{
    /**
     * @var \Magento\Framework\Image\AdapterFactory
     */
    private $adapterFactory;
    /**
     * @var \Magento\Framework\Filesystem
     */
    private $filesystem;
    /**
     * @var DirectoryList
     */
    private $directoryList;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    public function __construct(
        \Magento\Framework\Image\AdapterFactory $adapterFactory,
        \Magento\Framework\Filesystem $filesystem,
        DirectoryList $directoryList,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->adapterFactory = $adapterFactory;
        $this->filesystem = $filesystem;
        $this->directoryList = $directoryList;
        $this->storeManager = $storeManager;
    }

    public function resize($value, $fileName, $width = null, $height = null)
    {
        $imageAdapter = $this->adapterFactory->create();
        $imageAdapter->open($value["tmp_name"]);
        $imageAdapter->constrainOnly(TRUE);
        $imageAdapter->keepTransparency(TRUE);
        $imageAdapter->keepFrame(FALSE);
        $imageAdapter->keepAspectRatio(TRUE);
        $imageAdapter->resize(100,100);
        $mediaDirectory = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $imagepathfolder= 'test' . DIRECTORY_SEPARATOR;
        $destinationPath = $mediaDirectory->getAbsolutePath($imagepathfolder);
        $imageAdapter->save($destinationPath . $fileName);

        $resizedURL = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA). $imagepathfolder .  $fileName;
        return $resizedURL;
    }
}