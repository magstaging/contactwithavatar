<?php

namespace Mbs\ContactWithAvatar\Model;

use Magento\Framework\App\Filesystem\DirectoryList;

class ImageManipulation
{
    /**
     * @var \Magento\Framework\Filesystem
     */
    private $filesystem;
    /**
     * @var \Magento\Framework\Image\AdapterFactory
     */
    private $imageFactory;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var \Magento\Framework\Api\Uploader
     */
    private $uploader;
    /**
     * @var \Magento\Framework\Filesystem\Directory\WriteInterface
     */
    private $mediaDirectory;

    public function __construct(
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Image\AdapterFactory $imageFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Api\Uploader $uploader
    ) {
        $this->filesystem = $filesystem;
        $this->imageFactory = $imageFactory;
        $this->storeManager = $storeManager;
        $this->uploader = $uploader;
        $this->mediaDirectory = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA);
    }

    // pass imagename, width and height
    public function resize($image, $fileName, $width = null, $height = null)
    {
        $this->uploader->processFileAttributes([
            'tmp_name' => $image['tmp_name'],
            'name' => $fileName
        ]);
        $this->uploader->setFilesDispersion(false);
        $this->uploader->setFilenamesCaseSensitivity(false);
        $this->uploader->setAllowRenameFiles(true);
        $this->uploader->save($this->mediaDirectory->getAbsolutePath('tmp'), $fileName);

        $imageResized = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('test') . DIRECTORY_SEPARATOR . $fileName;
        $imageResize = $this->imageFactory->create();
        $imageResize->open($this->mediaDirectory->getAbsolutePath('tmp') . DIRECTORY_SEPARATOR . $fileName);
        $imageResize->constrainOnly(TRUE);
        $imageResize->keepTransparency(TRUE);
        $imageResize->keepFrame(FALSE);
        $imageResize->keepAspectRatio(TRUE);
        $imageResize->resize($width,$height);
        //destination folder
        $destination = $imageResized ;
        //save image
        $imageResize->save($destination);

        $resizedURL = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA). 'test' . DIRECTORY_SEPARATOR .  $fileName;
        return $resizedURL;
    }
}