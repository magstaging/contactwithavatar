<?php

namespace Mbs\ContactWithAvatar\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem\DirectoryList;
use Psr\Log\LoggerInterface;

class Post extends \Magento\Framework\App\Action\Action implements HttpPostActionInterface
{
    /**
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    private $dataPersistor;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var \Mbs\ContactWithAvatar\Model\ImageManipulation
     */
    private $imageManipulation;
    /**
     * @var \Mbs\ContactWithAvatar\Model\NirajPatelImageManipulator
     */
    private $nirajPatelImageManipulator;

    public function __construct(
        Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        LoggerInterface $logger,
        \Mbs\ContactWithAvatar\Model\ImageManipulation $imageManipulation,
        \Mbs\ContactWithAvatar\Model\NirajPatelImageManipulator $nirajPatelImageManipulator
    ) {
        parent::__construct($context);
        $this->dataPersistor = $dataPersistor;
        $this->logger = $logger;
        $this->imageManipulation = $imageManipulation;
        $this->nirajPatelImageManipulator = $nirajPatelImageManipulator;
    }

    /**
     * @inheritDoc
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        if (!$this->getRequest()->isPost()) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }
        try {
            $file = $this->getRequest()->getFiles('avatar');
            $nickname = $this->getRequest()->getParam('nickname');

            if (!empty($file))
            {
                $filename = sprintf('%s-%s', strtolower($nickname), $file['name']);
                //$imageResized = $this->imageManipulation->resize($file, $filename, 100, 100);

                $nirajPatelImageResized = $this->nirajPatelImageManipulator->resize($file, $filename, 100, 100);
            }

            $this->messageManager->addSuccessMessage(
                __('Thanks for sending us your Avatar ' )
            );
            $this->dataPersistor->clear('avator_data');
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->dataPersistor->set('avator_data', $this->getRequest()->getParams());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            $this->messageManager->addErrorMessage(
                __('An error occurred while processing your form. Please try again later.')
            );
            $this->dataPersistor->set('avator_data', $this->getRequest()->getParams());
        }
        return $this->resultRedirectFactory->create()->setPath('/');
    }

}
