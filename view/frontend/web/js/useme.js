define([
    "jquery"
], function ($) {
    'use strict';

    return function(config, element) {
        $("#custom-form").on('submit', function (e) {
            var self = this;
            e.preventDefault();

            var formData = new FormData(this);

            $.ajax({
                type: 'post',
                url: $(this).attr('action'),
                data: formData,
                contentType: false,
                processData: false,

                beforeSend: function () {
                    $('body').trigger('processStart');
                },
                success: function (res) {
                    console.log('success');
                    var eventData, parameters;

                    $('body').trigger('processStop');

                    location.reload();
                }
            });
        });
    }

});