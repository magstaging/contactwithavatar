# README #

This module renders a form on every page of the site. When submitting this form, it uploads an image resizing it without uploading beforehand

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/ContactWithAvatar when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

